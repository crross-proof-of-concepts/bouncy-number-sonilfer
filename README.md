# POC: Números inflados (Bouncy Number)

La presente prueba de concepto consiste en implementar un algoritmo que permita encontrar el menor número inflado (bouncy number) para el cual se cumple el porcentaje dado como entrada en la ejecución de la prueba.

### Estructura de proyecto
  - **receivers:** Contiene todos las entradas de la aplicación (Servicios web, conexiones a colas, etc...).
  - **use_cases:** Contiene todas las funcionalidades del negocio.
  - **applications:** Contiene el script o los scripts que inicial la aplicación.

### Tecnologías
  * [Python]

[Python]: <https://www.python.org/>
