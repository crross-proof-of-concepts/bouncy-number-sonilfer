import unittest
from use_cases.evaluation_number_use_case import EvaluationNumberUseCase


class TestEvaluationNumberUseCase(unittest.TestCase):

    def test_calculate_percent_50(self):
        use_cases = EvaluationNumberUseCase()
        return self.assertEqual(538, use_cases.least_number_bouncy_by_percent(50))

    def test_calculate_percent_90(self):
        use_cases = EvaluationNumberUseCase()
        return self.assertEqual(21780, use_cases.least_number_bouncy_by_percent(90))


if __name__ == "__main__":
    unittest.main()
