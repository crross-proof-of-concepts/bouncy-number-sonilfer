from enum import Enum


class NumberType(Enum):
    UNDEFINED = 0
    INCREASE = 1
    DECREASE = 2
    EQUALS = 3


class EvaluationNumberUseCase:

    def least_number_bouncy_by_percent(self, target_percentage):
        current_percentage = 0
        total_number_evaluated = 1
        total_bouncy_number = 0

        while current_percentage < target_percentage:
            if self.__is_bouncy_number(total_number_evaluated):
                total_bouncy_number += 1
            current_percentage = self.__calculate_percent(total_number_evaluated, total_bouncy_number)
            total_number_evaluated += 1

        return total_number_evaluated - 1

    def __calculate_percent(self, total_value, partial_value):
        return (float(partial_value) / float(total_value)) * float(100)

    def __is_bouncy_number(self, number):
        digits = [n for n in str(number)]
        prev = digits[0]
        prev_number_type = NumberType.UNDEFINED

        if number < 100:
            return False

        for index, val in enumerate(digits):
            if index == 0:
                continue

            current_number_type = self.__increase_or_decrease(prev, digits[index])
            prev = digits[index]

            if prev_number_type == NumberType.UNDEFINED and current_number_type != NumberType.EQUALS:
                prev_number_type = current_number_type
            elif prev_number_type != current_number_type and current_number_type != NumberType.EQUALS:
                return True

        return False

    def __increase_or_decrease(self, prev, actual):
        if int(prev) == int(actual):
            return NumberType.EQUALS
        elif int(prev) < int(actual):
            return NumberType.INCREASE
        else:
            return NumberType.DECREASE
