from use_cases.evaluation_number_use_case import EvaluationNumberUseCase


class NumberCli:
    use_case: EvaluationNumberUseCase

    def __init__(self):
        self.use_case = EvaluationNumberUseCase()

    def find_least_number_bouncy_by_percentage(self):
        percentage = int(input("Digite un porcentage:"))
        print(self.use_case.least_number_bouncy_by_percent(percentage))
